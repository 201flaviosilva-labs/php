<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nav</title>
  <style>
    nav {
      display: block;
      background-color: #333;
      font-family: Verdana;
      padding: 0.5em;
    }

    nav a {
      background-color: #999;
      color: #fff;
      padding: 0.2em;
      text-decoration: none;
    }

    nav a:hover {
      background-color: #666;
      padding: 0.5em 0.2em 0.5em 0.2em
    }
  </style>
</head>

<body>
  <?
  $menu = array(
    'home'  => array('text' => 'Home',  'url' => '?p=home'),
    'away'  => array('text' => 'Away',  'url' => '?p=away'),
    'about' => array('text' => 'About', 'url' => '?p=about'),
  );
  class CNavigation
  {
    function generateMenu($items)
    {
      $html = "<nav>\n";
      foreach ($items as $item) {
        $html .= "<a href='{$item['url']}'>{$item['text']}</a>\n";
      }
      $html .= "</nav>\n";
      return $html;
    }
  };

  echo CNavigation::GenerateMenu($menu);
  ?>

</body>

</html>
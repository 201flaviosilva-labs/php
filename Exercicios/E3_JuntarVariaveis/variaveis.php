<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html {
            width: 100%;
            height: 100%;
        }

        body {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-items: center;
            background: royalblue;
        }

        div {
            width: 500px;
            height: 400px;
            margin-left: 35%;
        }

        h4 {
            border-top-right-radius: 6px;
            border-top-left-radius: 6px;
            width: 100%;
            text-align: center;
            background: gray;
        }

        section {
            height: 100%;
            width: 100%;
            box-sizing: border-box;
            border-top: 1px solid black;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
            background: #95a4bd;
            padding: 4px 8px;
        }
    </style>
    <title>Juntar variaveis</title>
</head>

<body>
    <div>
        <h4>PHP</h4>
        <section>
            <?php
            $nome = "Silva";
            $idade = 25;

            echo "Nome: " . $nome . ", idade: " . $idade;
            echo "<br>";
            echo "Nome:  $nome, idade:  $idade";
            ?>
        </section>
    </div>
</body>

</html>
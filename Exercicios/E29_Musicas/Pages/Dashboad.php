<?php
require "../Data/DataConection.php";
require "../Data/SESSION.php";
$user_Id = $_SESSION["id"];
$sql = "SELECT * FROM Musicas INNER JOIN User ON Musicas.id = User.id WHERE $user_Id = User.id;";
$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Dashboard</title>
</head>

<body>
    <style>
        body {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    </style>
    <h1>Dashboard</h1>
    <main>
        <form action="../Data/Musicas/Criar.php" method="GET">
            <div class="form-group">
                <label for="albumInput">Album</label>
                <input type="text" name="Album" placeholder="Nome Album" class="form-control" id="albumInput" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="ArtistaInput">Artista</label>
                <input type="text" name="Artista" placeholder="Nome Artista" class="form-control" id="ArtistaInput" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="MúsicaInput">Musica</label>
                <input type="text" name="Musica" placeholder="Nome Música" class="form-control" id="MúsicaInput" aria-describedby="emailHelp">
            </div>

            <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Criar</button>
        </form>
        <hr>
        <table class="table table-striped table-responsive table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">User Id</th>
                    <th scope="col">Musica Id</th>
                    <th scope="col">Album</th>
                    <th scope="col">Artista</th>
                    <th scope="col">Nome Música</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>

            <tbody>
                <?php
                if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) { ?>
                        <tr>
                            <td> <?php echo $row["id"]; ?></td>
                            <td> <?php echo $row["Musicas_Id"]; ?></td>
                            <td> <?php echo $row["Album"]; ?></td>
                            <td> <?php echo $row["Artista"]; ?> </td>
                            <td> <?php echo $row["Musica"]; ?></td>
                            <td>
                                <a href="./recebido.php?Musicas_Id=<?php echo $row['Musicas_Id']; ?>" class="btn btn-success">Adicionar</a>
                                <a href="./recebido.php?Musicas_Id=<?php echo $row['Musicas_Id']; ?>" class="btn btn-info">Ver</a>
                                <a href="../Data/Musicas/Apagar.php?Musicas_Id=<?php echo $row['Musicas_Id']; ?>" class="btn btn-danger">Apagar</a>
                            </td>
                        </tr>
                <?php }
                } else {
                    echo "0 results";
                }
                ?>
            </tbody>
        </table>
    </main>
</body>

</html>
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Musicas";

// Criar conecção
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar a conecção
if ($conn->connect_error) {
    die("Foi este o problema: " . $conn->connect_error);
}

echo "Ligado à base de dados: " . $dbname;
echo "<br/>";

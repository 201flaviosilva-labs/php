<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resposta</title>
</head>

<body>

    <?php
    $PNome = isset($_POST["PNome"]) ? $_POST["PNome"] : "Indefenido";
    $UNome = isset($_POST["UNome"]) ? $_POST["UNome"] : "Indefenido";
    $EmailUser = isset($_POST["EmailUser"]) ? $_POST["EmailUser"] : "Indefenido";
    $PPasseUser = isset($_POST["PPasseUser"]) ? $_POST["PPasseUser"] : "Indefenido";


    $encriptado = base64_encode($PPasseUser);


    echo "<br/>";
    echo "Primeiro Nome -> $PNome <br/>";
    echo "Último Nome -> $UNome <br/>";
    echo "Email -> $EmailUser <br/>";
    echo "Palavra-Passe -> $PPasseUser <br/>";
    echo "Palavra-Passe Encriptado -> $encriptado <br/>";
    echo "Palavra-Passe Desencriptado ->" . base64_decode($encriptado) . "<br/>";
    ?>

    <button onclick="window.history.go(-1)">Voltar</button>

</body>

</html>
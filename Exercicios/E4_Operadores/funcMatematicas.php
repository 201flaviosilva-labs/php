<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html {
            width: 100%;
            height: 100%;
        }

        body {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-items: center;
            background: royalblue;
        }

        div {
            width: 500px;
            height: 400px;
            margin-left: 35%;
        }

        h4 {
            border-top-right-radius: 6px;
            border-top-left-radius: 6px;
            width: 100%;
            text-align: center;
            background: gray;
        }

        section {
            height: 100%;
            width: 100%;
            padding: 4px 8px;
            overflow: auto;
            box-sizing: border-box;
            border-top: 1px solid black;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
            background: #95a4bd;
        }
    </style>
    <title>PHP</title>
</head>

<body>
    <div>
        <h4>Funcões Matematicas</h4>
        <section>
            <?php
            $num1 = 9;
            $num2 = -4;
            $num3 = 2;
            $num4 = 5.4;
            $num5 = 5.52;
            $num6 = 1234567.8910;

            // abs
            echo "<h2> Valor Absoluto </h2>";
            echo "abs($num1) = ". abs($num1);
            echo "<br/>";
            echo "abs($num2) = ". abs($num2);
            echo "<br/>";

            // pow
            echo "<h2> Valor potencia </h2>";
            echo "pow($num1, $num3) = ". pow($num1, $num3);
            echo "<br/>";

            // sqrt
            echo "<h2> Valor Raiz Quadrada </h2>";
            echo "sqrt($num1) = ". sqrt($num1);
            echo "<br/>";

            // round
            echo "<h2> Valor Arredondar </h2>";
            echo "round($num4) = ". round($num4);
            echo "<br/>";
            echo "round($num5) = ". round($num5);
            echo "<br/>";

            // floor
            echo "<h2> Valor Arredondar por defeito </h2>";
            echo "floor($num4) = ". floor($num4);
            echo "<br/>";
            echo "floor($num5) = ". floor($num5);
            echo "<br/>";

            // ceil
            echo "<h2> Valor Arredondar para cima </h2>";
            echo "ceil($num4) = ". ceil($num4);
            echo "<br/>";
            echo "ceil($num5) = ". ceil($num5);
            echo "<br/>";

            // intval
            echo "<h2> Valor inteiro de uma variável </h2>";
            echo "intval($num4) = ". intval($num4);
            echo "<br/>";
            echo "intval($num5) = ". intval($num5);
            echo "<br/>";

            // number_format
            echo "<h2> Formatação de Números (casas decimais) </h2>";
            echo 'number_format(variavel, nº_casas_decimais, "separador_de_inteiro", "separador_de_milhar"), não é necessario mandar todos os parametros';
            echo "<br/>";
            echo "number_format($num6, 4, ',', '.') = ". number_format($num6, 4, ",",".");
            echo "<br/>";
            echo "Exemplo mais fixe:";
            echo "<br/>";
            echo "Preço: ". number_format($num4,2), "€";
            echo "<br/>";
            ?>
        </section>
    </div>
</body>

</html>
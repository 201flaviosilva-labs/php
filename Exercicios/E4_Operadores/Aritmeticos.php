<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html {
            width: 100%;
            height: 100%;
        }

        body {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-items: center;
            background: royalblue;
        }

        div {
            width: 500px;
            height: 400px;
            margin-left: 35%;
        }

        h4 {
            border-top-right-radius: 6px;
            border-top-left-radius: 6px;
            width: 100%;
            text-align: center;
            background: gray;
        }

        section {
            height: 100%;
            width: 100%;
            padding: 4px 8px;
            overflow: auto;
            box-sizing: border-box;
            border-top: 1px solid black;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
            background: #95a4bd;
        }
    </style>
    <title>PHP</title>
</head>

<body>
    <div>
        <h4>Op. Aritemeticos</h4>
        <section>
            <?php
            $num1 = 5;
            $num2 = 2;
            $soma = $num1 + $num2;
            $sub = $num1 - $num2;
            $mul = $num1 * $num2;
            $div = $num1 / $num2;
            $rest = $num1 % $num2;

            $float1 = 1.5;
            // Incremento
            $float1++;
            --$float1; 
            $float2 = 4.3;
            //  Decremento
            $float2--;
            --$float2;
            $soma2 = $float1 + $float2;

            $float3 = 1.4;
            $num3 = 2;
            $soma3 = $float3 + $num3;

            echo "<h2> Sem Variaveis </h2>";
            echo "1 + 2 = ", 1 + 2;
            echo "<h2> Com variaveis </h2>";
            echo "$num1 + $num2 = $soma";
            echo "<h2> Float </h2>";
            echo "$float1 + $float2 = $soma2";
            echo "<h2> Float + Int </h2>";
            echo "$num3 + $float3 = $soma3";
            echo "<hr/>";
            echo "<h2> Soma: </h2>";
            echo "$num1 + $num2 = $soma";
            echo "<h2> Subtração: </h2>";
            echo "$num1 - $num2 = $sub";
            echo "<h2> Multiplicação: </h2>";
            echo "$num1 * $num2 = $mul";
            echo "<h2> Divisão: </h2>";
            echo "$num1 / $num2 = $div";
            echo "<h2> Resto: </h2>";
            echo "$num1 % $num2 = $rest";
            ?>
        </section>
    </div>
</body>

</html>
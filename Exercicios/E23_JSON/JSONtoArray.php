<?php

$json = '{ "titulo": "The Road", "ano": 2009, "sinops": "A América é uma sombra sinistra e cinzenta de si mesma após uma catástrofe", "horarios": ["10:00", "12:00", "15:00", "17:00", "19:00", "20:00", "22:00", "23:30", "02:00"] }';

$jsonEmPHP = json_decode($json);

echo var_dump($jsonEmPHP);

echo "Titulo:" . $jsonEmPHP->titulo . "\n"; // Uma opção
echo "Sinops:" . $jsonEmPHP->{"sinops"} . "\n"; // Outra opção
echo "Ano:" . $jsonEmPHP->ano . "\n";
$horariosString;
for ($i = 0; $i < count($jsonEmPHP->horarios); $i++) {
    $horariosString .= " <-> " . $jsonEmPHP->horarios[$i];
}
echo "Horarios:" . $horariosString . "\n";

<?php
$Strings = "1234";
echo "Código: $Strings";
echo "\r\n \r\n";

$s = sha1("$Strings");
echo "sha1: " . $s;
echo "\r\n \r\n";

$m = md5("$Strings");
echo "md5: " . $m;
echo "\r\n \r\n";

$b = base64_encode("$Strings");
echo "base64 codificado: " . $b;
echo "\r\n";
echo "base64 descodificado: " . base64_decode($b);
echo "\r\n \r\n";

// Mais Seguro
$h = password_hash("$Strings", PASSWORD_DEFAULT);
echo "password_hash codificado: " . $h;
echo "\r\n";
echo "password_hash descodificado: " . password_verify("$Strings", $h);

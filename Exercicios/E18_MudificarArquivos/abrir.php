<?php

gravar(date("d/m/y h:i:sa l"), "./Exercicios/E18_MudificarArquivos/text.txt");
echo ler("./Exercicios/E18_MudificarArquivos/text.txt");

function gravar($texto, $arquivo)
{
    $fp = fopen($arquivo, "a+");
    fwrite($fp, "$texto \r\n");
    fclose($fp);
}

function ler($arquivo)
{
    $fp = fopen($arquivo, "r");
    $texto =  fread($fp, filesize($arquivo));
    fclose($fp);
    return $texto;
}

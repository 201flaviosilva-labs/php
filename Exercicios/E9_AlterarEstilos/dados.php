<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $txt = isset($_GET["txt"]) ? $_GET["txt"] : "Exemplo";
    $num = isset($_GET["num"]) ? $_GET["num"] : "20";
    $favcolor = isset($_GET["favcolor"]) ? $_GET["favcolor"] : "#f00";
    ?>
    <style>
        span {
            font-size: <?php echo  $num; ?>px;
            color: <?php echo  $favcolor; ?>;
        }
    </style>
    <title>Dados</title>
</head>

<body>
    <?php
    echo "<h1> Resposta </h1>";
    echo "Texto = ". $txt;
    echo "<br/>";
    echo "Numero = ". $num;
    echo "<br/>";
    echo "Cor = ". $favcolor;
    echo "<br/>";
    echo "<span> $txt </span>";
    echo "<br/>";
    ?>
    <a href="http://127.0.0.1:5500/Exercicios/E9_AlterarEstilos/index.html">Voltar</a>
</body>

</html>
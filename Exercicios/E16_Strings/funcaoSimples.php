<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html {
            width: 100%;
            height: 100%;
        }

        body {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-items: center;
            background: royalblue;
        }

        div {
            width: 500px;
            height: 400px;
            margin-left: 35%;
        }

        h4 {
            border-top-right-radius: 6px;
            border-top-left-radius: 6px;
            width: 100%;
            text-align: center;
            background: gray;
        }

        section {
            height: 100%;
            width: 100%;
            padding: 4px 8px;
            overflow: auto;
            box-sizing: border-box;
            border-top: 1px solid black;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
            background: #95a4bd;
        }
    </style>
    <title>PHP</title>
</head>

<body>
    <div>
        <h4>Strings</h4>
        <section>
            <form>
                <?php
                $prod = "leite";
                $num1 = 5.3;
                $array[0] = 1;
                $array[1] = 2;
                $array[2] = 3;
                $array[3] = 4;
                $array2 = array (2,3,4,5,6,);

                printf("O %s está com o preço de %.2f ", $prod, $num1);
                echo "<br/>";
                print_r($array);
                echo "<br/>";
                print_r($array2);
                ?>
            </form>
        </section>
    </div>
</body>

</html>